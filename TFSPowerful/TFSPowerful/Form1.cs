﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.TestManagement.Client;
using Microsoft.TeamFoundation.WorkItemTracking.Proxy;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace TFSPowerful
{
	public partial class Form1 : Form
	{
		TfsTeamProjectCollection _tfctc;
		ITestManagementService _testmanagementService;
		ITestManagementTeamProject _teamProject;
		List<string> _attachmentFiles = null;

		public Form1()
		{
			InitializeComponent();
			InitTfsManagementService();
		}

		private void InitTfsManagementService()
		{
			_tfctc = new TfsTeamProjectCollection(new Uri("https://tfs.eu.exactsoftware.com:8088/tfs/EUCollection"));
			_testmanagementService = _tfctc.GetService<ITestManagementService>();
			_teamProject = _testmanagementService.GetTeamProject("ExactOnline");
		}

		private void btnGetInfo_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtPath.Text))
			{
				MessageBox.Show("Please define the path to store all the files");
			}
			if (!string.IsNullOrEmpty(txtRunId.Text))
			{
				var filePath = txtPath.Text;
				if (!Directory.Exists(filePath))
				{
					Directory.CreateDirectory(filePath);
				}

				ITestRun testRun = _teamProject.TestRuns.Find(int.Parse(txtRunId.Text));
				if(testRun != null && testRun.Attachments.Count > 0)
				{
					WorkItemServer wiserver = _tfctc.GetService<WorkItemServer>();
					_attachmentFiles = new List<string>();
					foreach(var attachment in testRun.Attachments)
					{
						_attachmentFiles.Add($"{filePath}\\{attachment.Name}");
						attachment.DownloadToFile($"{filePath}\\{attachment.Name}");
					}
					ReadAllFileAndExportResult(_attachmentFiles,txtTargetName.Text);
				}
			}
		}

		private void ReadAllFileAndExportResult(List<string>attachmentFiles,string filterTestName = null)
		{
			List<DataModel> results = new List<DataModel>();
			foreach (var attachment in attachmentFiles)
			{
				XDocument xDocument = XDocument.Load(attachment);
				XNamespace xnamespace = @"http://microsoft.com/schemas/VisualStudio/TeamTest/2010";
				var failedTests = (from data in xDocument.Descendants(xnamespace + "Results")
									   select data).Descendants(xnamespace + "UnitTestResult")
										  .Where(e => e.Attribute("outcome") != null && e.Attribute("outcome").Value == "Failed");
				var unitTestResults = failedTests.Where(e => e.Attribute("testName") != null && e.Attribute("testName").Value.Contains(string.IsNullOrEmpty(filterTestName) ? "" : filterTestName))?.ToList();
				if (unitTestResults != null && unitTestResults.Count > 0)
				{
					foreach (var item in unitTestResults)
					{
						var data = new DataModel();
						data.TrxFileName = attachment;
						data.TestName = item.Attribute("testName").Value;
						data.ErrorMessage = item.Descendants(xnamespace + "Message").FirstOrDefault()?.Value ?? "";
						data.StackTrace = item.Descendants(xnamespace + "StackTrace").FirstOrDefault()?.Value??"";
						results.Add(data);
					}
				}
			}
			CreateExcelFile($"{txtPath.Text}\\Results.xlsx", results);
		}

		private void CreateExcelFile(string filePath, List<DataModel> listData)
		{
			using (SpreadsheetDocument spreedDoc = SpreadsheetDocument.Create(filePath,
				DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
			{
				WorkbookPart wbPart = spreedDoc.WorkbookPart;
				if (wbPart == null)
				{
					wbPart = spreedDoc.AddWorkbookPart();
					wbPart.Workbook = new Workbook();
				}

				string sheetName = "Result";
				WorksheetPart worksheetPart = null;
				worksheetPart = wbPart.AddNewPart<WorksheetPart>();
				var sheetData = new SheetData();

				worksheetPart.Worksheet = new Worksheet(sheetData);

				if (wbPart.Workbook.Sheets == null)
				{
					wbPart.Workbook.AppendChild<Sheets>(new Sheets());
				}

				var sheet = new Sheet()
				{
					Id = wbPart.GetIdOfPart(worksheetPart),
					SheetId = 1,
					Name = sheetName
				};

				var workingSheet = ((WorksheetPart)wbPart.GetPartById(sheet.Id)).Worksheet;
				sheetData.AppendChild(CreateRowHeader());

				int rowindex = 2;
				foreach (var data in listData)
				{
					Row row = new Row();
					row.RowIndex = (UInt32)rowindex;
					
					row.AppendChild(AddCellWithText(data.TrxFileName));
					row.AppendChild(AddCellWithText(data.TestName));
					row.AppendChild(AddCellWithText(data.ErrorMessage));
					row.AppendChild(AddCellWithText(data.StackTrace));
					sheetData.AppendChild(row);
					rowindex++;
				}

				wbPart.Workbook.Sheets.AppendChild(sheet);

				//Set Border 
				//wbPark
				wbPart.Workbook.Save();
				MessageBox.Show("Completed");
			}
		}
		private Row CreateRowHeader()
		{
			Row rowHeader = new Row();
			rowHeader.RowIndex = 1;
			rowHeader.AppendChild(AddCellWithText("TrxFileName"));
			rowHeader.AppendChild(AddCellWithText("Test Name"));
			rowHeader.AppendChild(AddCellWithText("Error Message"));
			rowHeader.AppendChild(AddCellWithText("Error Stack Trace"));
			return rowHeader;
		}
		private Cell AddCellWithText(string text)
		{
			Cell c1 = new Cell();
			c1.DataType = CellValues.InlineString;

			InlineString inlineString = new InlineString();
			Text t = new Text();
			t.Text = text;
			inlineString.AppendChild(t);
			c1.AppendChild(inlineString);
			return c1;
		}
	}
	public class DataModel
	{
		public string TrxFileName { get; set; }
		public string TestName { get; set; }
		public string ErrorMessage { get; set; }
		public string StackTrace {get;set; }
	}
}
