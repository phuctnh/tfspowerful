﻿namespace TFSPowerful
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.txtRunId = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnGetInfo = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtTargetName = new System.Windows.Forms.TextBox();
			this.txtPath = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// txtRunId
			// 
			this.txtRunId.Location = new System.Drawing.Point(85, 16);
			this.txtRunId.Name = "txtRunId";
			this.txtRunId.Size = new System.Drawing.Size(195, 20);
			this.txtRunId.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(16, 19);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(63, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Test RunId:";
			// 
			// btnGetInfo
			// 
			this.btnGetInfo.Location = new System.Drawing.Point(85, 92);
			this.btnGetInfo.Name = "btnGetInfo";
			this.btnGetInfo.Size = new System.Drawing.Size(75, 23);
			this.btnGetInfo.TabIndex = 4;
			this.btnGetInfo.Text = "Find";
			this.btnGetInfo.UseVisualStyleBackColor = true;
			this.btnGetInfo.Click += new System.EventHandler(this.btnGetInfo_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.txtTargetName);
			this.groupBox1.Controls.Add(this.txtPath);
			this.groupBox1.Controls.Add(this.btnGetInfo);
			this.groupBox1.Controls.Add(this.txtRunId);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(523, 125);
			this.groupBox1.TabIndex = 6;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Setup Information";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(10, 69);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Target Name:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(47, 43);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(32, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Path:";
			// 
			// txtTargetName
			// 
			this.txtTargetName.Location = new System.Drawing.Point(85, 66);
			this.txtTargetName.Name = "txtTargetName";
			this.txtTargetName.Size = new System.Drawing.Size(195, 20);
			this.txtTargetName.TabIndex = 3;
			// 
			// txtPath
			// 
			this.txtPath.Location = new System.Drawing.Point(85, 40);
			this.txtPath.Name = "txtPath";
			this.txtPath.Size = new System.Drawing.Size(195, 20);
			this.txtPath.TabIndex = 2;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(547, 387);
			this.Controls.Add(this.groupBox1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
		private System.Windows.Forms.TextBox txtRunId;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnGetInfo;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtPath;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtTargetName;
	}
}

