﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSPowerful
{
    public class DownloadFilesResponse
    {
        public bool IsSuccess { get; set; }
        public bool IsError { get; set; }
        public List<string> FilesDownloaded { get; set; }
        public List<string> Errors { get; set; }
        public DownloadFilesResponse()
        {
            IsSuccess = true;
            Errors = new List<string>();
            FilesDownloaded = new List<string>();
        }
    }
}
